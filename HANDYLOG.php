<?php
/*
Plugin Name: HANDYLOG
Plugin URI: http://handypress.io
Description: Simple trace tool
Version: 1.0
Author: Yannick Armspach
Author Email: Yannick Armspach <yannick.armspach@gmail.com>
*/

/* Enable WP_DEBUG mode */
// define( 'WP_DEBUG', true );
// define( 'WP_DEBUG_LOG', true );
// define( 'WP_DEBUG_DISPLAY', false );
// @ini_set( 'display_errors', 0 );

/**
*
* HANDYLOG
*
**/
if ( ! class_exists('HANDYLOG') ) {

/**
*
* CONSTANT VAR
*
**/
define( 'HANDYLOG_FILE', __FILE__ );
define( 'HANDYLOG_DIR', plugin_dir_path( HANDYLOG_FILE ) );
define( 'HANDYLOG_URL', plugin_dir_url( HANDYLOG_FILE ) );

class HANDYLOG {

  public $add_log_meta;

  public $kill = false;

  public $show_log = true;

  function __construct(){

      add_action('wp', array( $this, 'create_log_meta' ), 9999999999999999999999999999 );

      add_action('init', array( $this, 'add_style'), 9999999999999999999999999999 );
      add_action('init', array( $this, 'add_script'), 9999999999999999999999999999 );

      // no output on ajax call
      //if ( ! defined( 'DOING_AJAX' ) && ! DOING_AJAX ) add_action('shutdown', array( $this, 'logPrint'), 9999999999999999999999999999 );

      if ( ! isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) || ( empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') )  {

        add_action('shutdown', array( $this, 'logPrint'), 9999999999999999999999999999 );

      }

      add_action('shutdown', array( $this, 'logfilePrint'), 9999999999999999999999999999 );

      $this->check_server_cache();

  }

  public function add_style(){

    wp_enqueue_style( 'HANDYLOG-jsonview', HANDYLOG_URL . 'lib/jsonview/jquery.jsonview.css', false, false, 'screen' );

    wp_enqueue_style( 'HANDYLOG', HANDYLOG_URL . 'css/HANDYLOG.css', false, false, 'screen' );
  
  }

  public function add_script(){

    wp_enqueue_script('HANDYLOG-jsonview', HANDYLOG_URL . 'lib/jsonview/jquery.jsonview.js', array('jquery'), '1.0', true );

    wp_enqueue_script('HANDYLOG', HANDYLOG_URL . 'js/HANDYLOG.js', array('jquery'), '1.0', true );
  
  }

  public function log( $id, $data, $state ){

    if ( $id == 'kill' ) $this->kill = true;

    $this->state = $state;

    $this->log[] = array($id,$data);

  }

  public function create_log_meta(){

    global $post;

    if ( $this->add_log_meta ) {

      $data = get_post_meta( $post->ID,'',true );
      $data['_wp_page_template'] = get_post_meta( $post->ID, '_wp_page_template', true );

      $this->log['log_meta'] = array( 'get_post_meta('.$post->ID.')', $data );

    }

  }

  public function log_meta() {

    $this->add_log_meta = true;

  }

  public function logPrint(){

    //echo   '<script type="text/javascript" src="' . HANDYLOG_URL . 'lib/jsonview/jquery.jsonview.js' . '"></script>';
    //echo   '<script type="text/javascript" src="' . HANDYLOG_URL . 'js/HANDYLOG.js' . '"></script>';
    
    if ( $this->kill == false && isset($this->log) ) {

      if ( isset($this->add_log_meta) && $this->add_log_meta != true ) unset( $this->log['log_meta'] );

      echo '<div id="HANDYLOG-view" class="' . $this->state . '">';

        echo '<div id="HANDYLOG-bar"><span class="dashicons dashicons-editor-code"></span></div>';

        echo '<div class="HANDYLOG-items">';

          foreach ($this->log as $key => $log) {

              echo '<div class="HANDYLOG-item">';

                if ( is_string( $log[0] ) ) {
                  echo '<h3 class="HANDYLOG-item-title" >'. $log[0].'</h3>';
                } else {
                  echo '<h3 class="HANDYLOG-item-title" >-</h3>';
                }

                if ( is_array( $log[1] ) ) {

                  if ( $log[1] ) {

                    //json
                    echo '<div id="collapse-btn" class="HANDYLOG-item-title-action">Collapse All</div><div id="expand-btn" class="HANDYLOG-item-title-action">Expand All</div>';
                    echo '<div class="HANDYLOG-item-json" id="json">';
                      echo '<textarea>'. json_encode($log[1]) .'</textarea>';
                    echo '</div>';

                  } else {

                    //undefined
                    echo '<i class="HANDYLOG-undefined">undefined</i>';

                  }

                } else if ( is_object( $log[1] ) ) {

                  echo '<pre>'; var_dump( $log[1] ); echo '</pre>';

                } else {

                  if ( $log[1] ) {

                    //txt
                    echo '<textarea>'. $log[1] .'</textarea>';

                    //print_r
                    //echo '<div class="HANDYLOG-item"><b>'.$log[0].'<b><pre>'; print_r($log[1]); echo '</pre></div>';

                    //dump
                    //echo '<div style="border-left:solid 5px #000000;padding:10px;margin-bottom:10px;color:#232323"><b>$_POST<b><pre>'; var_dump($_POST); echo '</pre></div>';

                  } else {

                    //undefined
                    echo '<i class="HANDYLOG-undefined">undefined</i>';

                  }

                }

              echo '</div>';

          }

        echo '</div>';

      echo '</div>';

    }

  }

  public function logfilePrint(){

    if ( $this->show_log ) {

      $wplog = WP_CONTENT_DIR . '/debug.log';

      if (  file_exists( $wplog ) ) {

        $wplog_data = file_get_contents( $wplog );

        if ( $wplog_data ) {

          echo '<div id="HANDYLOG-logfile">';

            echo '<span class="HANDYLOG-logfile-close dashicons dashicons-no-alt"></span><div id="HANDYLOG-logfile-content">' . nl2br( $wplog_data ) . '<div>';

          echo '</div>';

        }

      }

    }

  }

  public function enable_log() {

    $this->show_log = true;

  }

  public function check_server_cache() {

    //disable cache
    // header("Cache-Control: max-age=1");

    // if ( is_callable( 'opcache_reset' ) ) {

    //   opcache_reset();

    // }

  }

}

//reset debug log
$wplog = WP_CONTENT_DIR . '/debug.log';
if ( file_exists( $wplog ) ) file_put_contents( $wplog, '' );

function HANDYLOG_DEBUG(){

  global $HANDYLOG;

  $HANDYLOG->enable_log();

}
function _HANDYLOG_DEBUG(){

  global $HANDYLOG;

  $HANDYLOG->enable_log();

}

function HANDYLOG( $id = '-', $data = null, $state = "HANDYLOG-hide" ){

  global $HANDYLOG;

  if ( $data == null && ( is_array( $id ) || is_object( $id ) ) ) $data = $id;

  $HANDYLOG->log($id,$data,$state);

}

// add _ before function to quick open state
function _HANDYLOG( $id = '-', $data = null, $state = "open" ){

  global $HANDYLOG;

  if ( $data == null && ( is_array( $id ) || is_object( $id ) ) ) $data = $id;

  $HANDYLOG->log($id,$data,$state);

}
function HANDYLOG_( $id = '-', $data = null, $state = "open" ){

  global $HANDYLOG;

  if ( $data == null && ( is_array( $id ) || is_object( $id ) ) ) $data = $id;

  $HANDYLOG->log($id,$data,$state);

}

function HANDYLOG_META(){

  global $HANDYLOG;

  $HANDYLOG->log_meta();

}

function HANDYLOG_POST(){

  global $HANDYLOG;

  $HANDYLOG->log('$_POST',$_POST,"HANDYLOG-hide");

}

function HANDYLOG_GET(){

  global $HANDYLOG;

  $HANDYLOG->log('$_GET',$_GET,"HANDYLOG-hide");

}

function hlog( $id = '-', $data = null, $state = "HANDYLOG-hide" ){

  HANDYLOG( $id, $data, $state );

}

$HANDYLOG = new HANDYLOG();

// _HANDYLOG_DEBUG();

}




?>
