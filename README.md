# HANDYLOG #
========

Trace any value in your WORDPRESS code!

Add HANDYLOG() function throw your code. The output is execute in the very last hook "shutdown" to print your value in a clean html console. 


### Global use : ###

**HANDYLOG(** **$title**(*string*), **$value**(*string|number|bool|array*) **);**

or 

**HANDYLOG(** **$value**(*string|number|bool|array*) **);**


### Preset use : ###

**HANDYLOG_META()**  : output all the current post meta data

**HANDYLOG_POST()**  : output the $_POST data

**HANDYLOG_GET()**   : output the $_GET data


========


![ScreenShot-1.png](https://bitbucket.org/repo/ybpL7z/images/3293374313-ScreenShot-1.png)

![ScreenShot-2.png](https://bitbucket.org/repo/ybpL7z/images/3348017218-ScreenShot-2.png)