(function($) {

  jQuery(document).ready(function(){

    $('.HANDYLOG-item-json').each(function(i, obj){

      var $this = $(this);
      var $json = $this.find('textarea').val();

      $this.JSONView( $json,{ collapsed: true, nl2br: true, recursive_collapser:false } ).JSONView('toggle', 0);


      $this.parent().find('#collapse-btn').hide();
      $this.parent().find('#expand-btn').show();

      $this.parent().find('#collapse-btn').on('click', function() {
        $this.JSONView('collapse');
        $this.parent().find('#collapse-btn').hide();
        $this.parent().find('#expand-btn').show();
      });

      $this.parent().find('#expand-btn').on('click', function() {
        $this.JSONView('expand');
        $this.parent().find('#collapse-btn').show();
        $this.parent().find('#expand-btn').hide();
      });

    });

    function resize_view() {

      $('html').css('margin-right', ( $('#HANDYLOG-view').width() ) + 'px' );

    }

    $('#HANDYLOG-bar').on('click', function() {

      if ( $('#HANDYLOG-view').hasClass('HANDYLOG-hide') ) {

        $('#HANDYLOG-view').removeClass('HANDYLOG-hide');

      } else {

        $('#HANDYLOG-view').addClass('HANDYLOG-hide');

      }

      resize_view();

    });

    $('html').on('click', '.HANDYLOG-item-title-action, .collapser', function(){

      resize_view();

    } );

    $('html').on('click', '.HANDYLOG-logfile-close', function(){

      $('#HANDYLOG-logfile').hide();

    } );

    resize_view();


  });


  }(jQuery));
